nix-install(){ nix-env -iA $1; }
nix-query(){ nix-env -qa \* -P | grep -i "$1"; }
nix-update(){ sudo nix-channel --update; }
nix-rebuild(){ sqlRestart; sudo nixos-rebuild switch; }
nix-query-package-path(){ ls /nix/store/*/ | grep "$1"; }
nix-update-local(){ nix-env -u;}
nix-query-local(){ nix-env -qa \* -P -f ~/sandbox/nixpkgs/ | grep -i "$1"; }
nix-install-local(){ nix-env -iA -f ~/sandbox/nixpkgs/ $1; }

installNix(){ nix-env -iA $1; }
queryNix(){ nix-env -qa \* -P | grep -i "$1"; }
updateNix(){ sudo nix-channel --update; }
rebuildNix(){ sqlRestart; sudo nixos-rebuild switch; }
queryPackagePath(){ ls /nix/store/*/ | grep "$1"; }
updateNixLocal(){ nix-env -u;}

sqlLog(){ journalctl -u postgresql --reverse; }
sqlLog10(){ journalctl -u postgresql --no-pager -a --lines=20; }
sqlRestart(){ sudo bash -c "systemctl stop postgresql; systemctl start postgresql;"; }

webLog(){ journalctl -u lighttpd --reverse; }
webLog10(){ journalctl -u lighttpd --no-pager -a --lines=20; }
webRestart(){ sudo bash -c "systemctl stop lighttpd; systemctl start lighttpd;"; }

alias-reload(){ source ~/.bashrc; }
alias-edit(){ gvim ~/.bashrc; }
alias-view(){ cat ~/.bashrc; }


# HELPERS
filterNotice(){ grep -v "NOTICE";}

sqitchRebaseLastHelper(){ sqitch revert @HEAD^1; sqitch deploy --verify; }
sqitchRebaseLast(){ sqitchRebaseLastHelper 2> >(filterNotice); }

conflicthandle(){ git mergetool -t kdiff3; }

function timer_start {
  timer=${timer:-$SECONDS}
}

function timer_stop {
  timer_show=$(($SECONDS - $timer))
  unset timer
}

#trap 'timer_start' DEBUG
#PROMPT_COMMAND=timer_stop

#PS1='[last: ${timer_show}s][\w]$ '

source ~/nixdotfiles/.bash-git-prompt/gitprompt.sh
GIT_PROMPT_ONLY_IN_REPO=1

export EDITOR=vim
