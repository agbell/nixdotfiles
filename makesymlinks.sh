DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ln -s $DIR/.bashrc ~/.bashrc --force
mkdir -p ~/.ghc/
ln -s $DIR/ghci.conf  ~/.ghc/ghci.conf --force
ln -s $DIR/.vimrc.bundles.local  ~/.vimrc.bundles.local --force
ln -s $DIR/.vimrc.local  ~/.vimrc.local --force
ln -s $DIR/.gitconfig  ~/.gitconfig --force
